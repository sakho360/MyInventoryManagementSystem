package com.persistance;

public class OrderReceiveInfo {

    private Integer orderReceiveID;
    private Integer s0Received;
    private Integer s1Received;
    private Integer s2Received;
    private Integer s3Received;
    private Integer s4Received;
    private Integer s5Received;
    private Integer s6Received;
    private Integer s7Received;
    private Integer s8Received;
    private Integer s9Received;
    private Double receiveMRP;
    private Integer receiveQuantity;
    private Double receiveSubTotal;
    private Character receiveStatus;
    private String receivedBy;
    private String receiveDate;

    private String orderNo;
    private String orderDate;
    private String itemCode;
    private Character sizeCode;

    private ProductOrderDetailsInfo productOrderDetailsInfo;

    /**
     * @return the orderReceiveID
     */
    public Integer getOrderReceiveID() {
        return orderReceiveID;
    }

    /**
     * @param orderReceiveID the orderReceiveID to set
     */
    public void setOrderReceiveID(Integer orderReceiveID) {
        this.orderReceiveID = orderReceiveID;
    }

    /**
     * @return the s0Received
     */
    public Integer getS0Received() {
        return s0Received;
    }

    /**
     * @param s0Received the s0Received to set
     */
    public void setS0Received(Integer s0Received) {
        this.s0Received = s0Received;
    }

    /**
     * @return the s1Received
     */
    public Integer getS1Received() {
        return s1Received;
    }

    /**
     * @param s1Received the s1Received to set
     */
    public void setS1Received(Integer s1Received) {
        this.s1Received = s1Received;
    }

    /**
     * @return the s2Received
     */
    public Integer getS2Received() {
        return s2Received;
    }

    /**
     * @param s2Received the s2Received to set
     */
    public void setS2Received(Integer s2Received) {
        this.s2Received = s2Received;
    }

    /**
     * @return the s3Received
     */
    public Integer getS3Received() {
        return s3Received;
    }

    /**
     * @param s3Received the s3Received to set
     */
    public void setS3Received(Integer s3Received) {
        this.s3Received = s3Received;
    }

    /**
     * @return the s4Received
     */
    public Integer getS4Received() {
        return s4Received;
    }

    /**
     * @param s4Received the s4Received to set
     */
    public void setS4Received(Integer s4Received) {
        this.s4Received = s4Received;
    }

    /**
     * @return the s5Received
     */
    public Integer getS5Received() {
        return s5Received;
    }

    /**
     * @param s5Received the s5Received to set
     */
    public void setS5Received(Integer s5Received) {
        this.s5Received = s5Received;
    }

    /**
     * @return the s6Received
     */
    public Integer getS6Received() {
        return s6Received;
    }

    /**
     * @param s6Received the s6Received to set
     */
    public void setS6Received(Integer s6Received) {
        this.s6Received = s6Received;
    }

    /**
     * @return the s7Received
     */
    public Integer getS7Received() {
        return s7Received;
    }

    /**
     * @param s7Received the s7Received to set
     */
    public void setS7Received(Integer s7Received) {
        this.s7Received = s7Received;
    }

    /**
     * @return the s8Received
     */
    public Integer getS8Received() {
        return s8Received;
    }

    /**
     * @param s8Received the s8Received to set
     */
    public void setS8Received(Integer s8Received) {
        this.s8Received = s8Received;
    }

    /**
     * @return the s9Received
     */
    public Integer getS9Received() {
        return s9Received;
    }

    /**
     * @param s9Received the s9Received to set
     */
    public void setS9Received(Integer s9Received) {
        this.s9Received = s9Received;
    }

    /**
     * @return the receiveMRP
     */
    public Double getReceiveMRP() {
        return receiveMRP;
    }

    /**
     * @param receiveMRP the receiveMRP to set
     */
    public void setReceiveMRP(Double receiveMRP) {
        this.receiveMRP = receiveMRP;
    }

    /**
     * @return the receiveQuantity
     */
    public Integer getReceiveQuantity() {
        return receiveQuantity;
    }

    /**
     * @param receiveQuantity the receiveQuantity to set
     */
    public void setReceiveQuantity(Integer receiveQuantity) {
        this.receiveQuantity = receiveQuantity;
    }

    /**
     * @return the receiveSubTotal
     */
    public Double getReceiveSubTotal() {
        return receiveSubTotal;
    }

    /**
     * @param receiveSubTotal the receiveSubTotal to set
     */
    public void setReceiveSubTotal(Double receiveSubTotal) {
        this.receiveSubTotal = receiveSubTotal;
    }

    /**
     * @return the receiveStatus
     */
    public Character getReceiveStatus() {
        return receiveStatus;
    }

    /**
     * @param receiveStatus the receiveStatus to set
     */
    public void setReceiveStatus(Character receiveStatus) {
        this.receiveStatus = receiveStatus;
    }

    /**
     * @return the receivedBy
     */
    public String getReceivedBy() {
        return receivedBy;
    }

    /**
     * @param receivedBy the receivedBy to set
     */
    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    /**
     * @return the receiveDate
     */
    public String getReceiveDate() {
        return receiveDate;
    }

    /**
     * @param receiveDate the receiveDate to set
     */
    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    /**
     * @return the orderNo
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo the orderNo to set
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return the orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate the orderDate to set
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the sizeCode
     */
    public Character getSizeCode() {
        return sizeCode;
    }

    /**
     * @param sizeCode the sizeCode to set
     */
    public void setSizeCode(Character sizeCode) {
        this.sizeCode = sizeCode;
    }

    /**
     * @return the productOrderDetailsInfo
     */
    public ProductOrderDetailsInfo getProductOrderDetailsInfo() {
        return productOrderDetailsInfo;
    }

    /**
     * @param productOrderDetailsInfo the productOrderDetailsInfo to set
     */
    public void setProductOrderDetailsInfo(ProductOrderDetailsInfo productOrderDetailsInfo) {
        this.productOrderDetailsInfo = productOrderDetailsInfo;
    }
}
