package com.persistance;

public class ProductReturnInfo {

    private Integer productReturnID;
    private String itemCode;
    private String invoiceNo;
    private String sizeUnit;
    private Integer returnSize0;
    private Integer returnSize1;
    private Integer returnSize2;
    private Integer returnSize3;
    private Integer returnSize4;
    private Integer returnSize5;
    private Integer returnSize6;
    private Integer returnSize7;
    private Integer returnSize8;
    private Integer returnSize9;
    private Integer returnQuantity;
    private String returnCause;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private Character returnStatus;
    private InvoiceMasterInfo invoiceMasterInfo;

    /**
     * @return the productReturnID
     */
    public Integer getProductReturnID() {
        return productReturnID;
    }

    /**
     * @param productReturnID the productReturnID to set
     */
    public void setProductReturnID(Integer productReturnID) {
        this.productReturnID = productReturnID;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * @return the sizeUnit
     */
    public String getSizeUnit() {
        return sizeUnit;
    }

    /**
     * @param sizeUnit the sizeUnit to set
     */
    public void setSizeUnit(String sizeUnit) {
        this.sizeUnit = sizeUnit;
    }

    /**
     * @return the returnSize0
     */
    public Integer getReturnSize0() {
        return returnSize0;
    }

    /**
     * @param returnSize0 the returnSize0 to set
     */
    public void setReturnSize0(Integer returnSize0) {
        this.returnSize0 = returnSize0;
    }

    /**
     * @return the returnSize1
     */
    public Integer getReturnSize1() {
        return returnSize1;
    }

    /**
     * @param returnSize1 the returnSize1 to set
     */
    public void setReturnSize1(Integer returnSize1) {
        this.returnSize1 = returnSize1;
    }

    /**
     * @return the returnSize2
     */
    public Integer getReturnSize2() {
        return returnSize2;
    }

    /**
     * @param returnSize2 the returnSize2 to set
     */
    public void setReturnSize2(Integer returnSize2) {
        this.returnSize2 = returnSize2;
    }

    /**
     * @return the returnSize3
     */
    public Integer getReturnSize3() {
        return returnSize3;
    }

    /**
     * @param returnSize3 the returnSize3 to set
     */
    public void setReturnSize3(Integer returnSize3) {
        this.returnSize3 = returnSize3;
    }

    /**
     * @return the returnSize4
     */
    public Integer getReturnSize4() {
        return returnSize4;
    }

    /**
     * @param returnSize4 the returnSize4 to set
     */
    public void setReturnSize4(Integer returnSize4) {
        this.returnSize4 = returnSize4;
    }

    /**
     * @return the returnSize5
     */
    public Integer getReturnSize5() {
        return returnSize5;
    }

    /**
     * @param returnSize5 the returnSize5 to set
     */
    public void setReturnSize5(Integer returnSize5) {
        this.returnSize5 = returnSize5;
    }

    /**
     * @return the returnSize6
     */
    public Integer getReturnSize6() {
        return returnSize6;
    }

    /**
     * @param returnSize6 the returnSize6 to set
     */
    public void setReturnSize6(Integer returnSize6) {
        this.returnSize6 = returnSize6;
    }

    /**
     * @return the returnSize7
     */
    public Integer getReturnSize7() {
        return returnSize7;
    }

    /**
     * @param returnSize7 the returnSize7 to set
     */
    public void setReturnSize7(Integer returnSize7) {
        this.returnSize7 = returnSize7;
    }

    /**
     * @return the returnSize8
     */
    public Integer getReturnSize8() {
        return returnSize8;
    }

    /**
     * @param returnSize8 the returnSize8 to set
     */
    public void setReturnSize8(Integer returnSize8) {
        this.returnSize8 = returnSize8;
    }

    /**
     * @return the returnSize9
     */
    public Integer getReturnSize9() {
        return returnSize9;
    }

    /**
     * @param returnSize9 the returnSize9 to set
     */
    public void setReturnSize9(Integer returnSize9) {
        this.returnSize9 = returnSize9;
    }

    /**
     * @return the returnQuantity
     */
    public Integer getReturnQuantity() {
        return returnQuantity;
    }

    /**
     * @param returnQuantity the returnQuantity to set
     */
    public void setReturnQuantity(Integer returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    /**
     * @return the returnCause
     */
    public String getReturnCause() {
        return returnCause;
    }

    /**
     * @param returnCause the returnCause to set
     */
    public void setReturnCause(String returnCause) {
        this.returnCause = returnCause;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the returnStatus
     */
    public Character getReturnStatus() {
        return returnStatus;
    }

    /**
     * @param returnStatus the returnStatus to set
     */
    public void setReturnStatus(Character returnStatus) {
        this.returnStatus = returnStatus;
    }

    /**
     * @return the invoiceMasterInfo
     */
    public InvoiceMasterInfo getInvoiceMasterInfo() {
        return invoiceMasterInfo;
    }

    /**
     * @param invoiceMasterInfo the invoiceMasterInfo to set
     */
    public void setInvoiceMasterInfo(InvoiceMasterInfo invoiceMasterInfo) {
        this.invoiceMasterInfo = invoiceMasterInfo;
    }
}
