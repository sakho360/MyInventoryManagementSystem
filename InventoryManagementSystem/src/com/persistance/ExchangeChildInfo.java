package com.persistance;

public class ExchangeChildInfo {

    private Integer exchangeChildID;
    private String oldItemCode;
    private String oldSizeUnit;
    private Integer oldSize0;
    private Integer oldSize1;
    private Integer oldSize2;
    private Integer oldSize3;
    private Integer oldSize4;
    private Integer oldSize5;
    private Integer oldSize6;
    private Integer oldSize7;
    private Integer oldSize8;
    private Integer oldSize9;
    private Integer oldQuantity;
    private Double oldMrp;
    private Double oldTotal;
    private String newItemCode;
    private String newSizeUnit;
    private Integer newSize0;
    private Integer newSize1;
    private Integer newSize2;
    private Integer newSize3;
    private Integer newSize4;
    private Integer newSize5;
    private Integer newSize6;
    private Integer newSize7;
    private Integer newSize8;
    private Integer newSize9;
    private Integer newQuantity;
    private Double newMrp;
    private Double newTotal;
    private String invoiceNo;

    /**
     * @return the exchangeChildID
     */
    public Integer getExchangeChildID() {
        return exchangeChildID;
    }

    /**
     * @param exchangeChildID the exchangeChildID to set
     */
    public void setExchangeChildID(Integer exchangeChildID) {
        this.exchangeChildID = exchangeChildID;
    }

    /**
     * @return the oldItemCode
     */
    public String getOldItemCode() {
        return oldItemCode;
    }

    /**
     * @param oldItemCode the oldItemCode to set
     */
    public void setOldItemCode(String oldItemCode) {
        this.oldItemCode = oldItemCode;
    }

    /**
     * @return the oldSizeUnit
     */
    public String getOldSizeUnit() {
        return oldSizeUnit;
    }

    /**
     * @param oldSizeUnit the oldSizeUnit to set
     */
    public void setOldSizeUnit(String oldSizeUnit) {
        this.oldSizeUnit = oldSizeUnit;
    }

    /**
     * @return the oldSize0
     */
    public Integer getOldSize0() {
        return oldSize0;
    }

    /**
     * @param oldSize0 the oldSize0 to set
     */
    public void setOldSize0(Integer oldSize0) {
        this.oldSize0 = oldSize0;
    }

    /**
     * @return the oldSize1
     */
    public Integer getOldSize1() {
        return oldSize1;
    }

    /**
     * @param oldSize1 the oldSize1 to set
     */
    public void setOldSize1(Integer oldSize1) {
        this.oldSize1 = oldSize1;
    }

    /**
     * @return the oldSize2
     */
    public Integer getOldSize2() {
        return oldSize2;
    }

    /**
     * @param oldSize2 the oldSize2 to set
     */
    public void setOldSize2(Integer oldSize2) {
        this.oldSize2 = oldSize2;
    }

    /**
     * @return the oldSize3
     */
    public Integer getOldSize3() {
        return oldSize3;
    }

    /**
     * @param oldSize3 the oldSize3 to set
     */
    public void setOldSize3(Integer oldSize3) {
        this.oldSize3 = oldSize3;
    }

    /**
     * @return the oldSize4
     */
    public Integer getOldSize4() {
        return oldSize4;
    }

    /**
     * @param oldSize4 the oldSize4 to set
     */
    public void setOldSize4(Integer oldSize4) {
        this.oldSize4 = oldSize4;
    }

    /**
     * @return the oldSize5
     */
    public Integer getOldSize5() {
        return oldSize5;
    }

    /**
     * @param oldSize5 the oldSize5 to set
     */
    public void setOldSize5(Integer oldSize5) {
        this.oldSize5 = oldSize5;
    }

    /**
     * @return the oldSize6
     */
    public Integer getOldSize6() {
        return oldSize6;
    }

    /**
     * @param oldSize6 the oldSize6 to set
     */
    public void setOldSize6(Integer oldSize6) {
        this.oldSize6 = oldSize6;
    }

    /**
     * @return the oldSize7
     */
    public Integer getOldSize7() {
        return oldSize7;
    }

    /**
     * @param oldSize7 the oldSize7 to set
     */
    public void setOldSize7(Integer oldSize7) {
        this.oldSize7 = oldSize7;
    }

    /**
     * @return the oldSize8
     */
    public Integer getOldSize8() {
        return oldSize8;
    }

    /**
     * @param oldSize8 the oldSize8 to set
     */
    public void setOldSize8(Integer oldSize8) {
        this.oldSize8 = oldSize8;
    }

    /**
     * @return the oldSize9
     */
    public Integer getOldSize9() {
        return oldSize9;
    }

    /**
     * @param oldSize9 the oldSize9 to set
     */
    public void setOldSize9(Integer oldSize9) {
        this.oldSize9 = oldSize9;
    }

    /**
     * @return the oldQuantity
     */
    public Integer getOldQuantity() {
        return oldQuantity;
    }

    /**
     * @param oldQuantity the oldQuantity to set
     */
    public void setOldQuantity(Integer oldQuantity) {
        this.oldQuantity = oldQuantity;
    }

    /**
     * @return the oldMrp
     */
    public Double getOldMrp() {
        return oldMrp;
    }

    /**
     * @param oldMrp the oldMrp to set
     */
    public void setOldMrp(Double oldMrp) {
        this.oldMrp = oldMrp;
    }

    /**
     * @return the oldTotal
     */
    public Double getOldTotal() {
        return oldTotal;
    }

    /**
     * @param oldTotal the oldTotal to set
     */
    public void setOldTotal(Double oldTotal) {
        this.oldTotal = oldTotal;
    }

    /**
     * @return the newItemCode
     */
    public String getNewItemCode() {
        return newItemCode;
    }

    /**
     * @param newItemCode the newItemCode to set
     */
    public void setNewItemCode(String newItemCode) {
        this.newItemCode = newItemCode;
    }

    /**
     * @return the newSizeUnit
     */
    public String getNewSizeUnit() {
        return newSizeUnit;
    }

    /**
     * @param newSizeUnit the newSizeUnit to set
     */
    public void setNewSizeUnit(String newSizeUnit) {
        this.newSizeUnit = newSizeUnit;
    }

    /**
     * @return the newSize0
     */
    public Integer getNewSize0() {
        return newSize0;
    }

    /**
     * @param newSize0 the newSize0 to set
     */
    public void setNewSize0(Integer newSize0) {
        this.newSize0 = newSize0;
    }

    /**
     * @return the newSize1
     */
    public Integer getNewSize1() {
        return newSize1;
    }

    /**
     * @param newSize1 the newSize1 to set
     */
    public void setNewSize1(Integer newSize1) {
        this.newSize1 = newSize1;
    }

    /**
     * @return the newSize2
     */
    public Integer getNewSize2() {
        return newSize2;
    }

    /**
     * @param newSize2 the newSize2 to set
     */
    public void setNewSize2(Integer newSize2) {
        this.newSize2 = newSize2;
    }

    /**
     * @return the newSize3
     */
    public Integer getNewSize3() {
        return newSize3;
    }

    /**
     * @param newSize3 the newSize3 to set
     */
    public void setNewSize3(Integer newSize3) {
        this.newSize3 = newSize3;
    }

    /**
     * @return the newSize4
     */
    public Integer getNewSize4() {
        return newSize4;
    }

    /**
     * @param newSize4 the newSize4 to set
     */
    public void setNewSize4(Integer newSize4) {
        this.newSize4 = newSize4;
    }

    /**
     * @return the newSize5
     */
    public Integer getNewSize5() {
        return newSize5;
    }

    /**
     * @param newSize5 the newSize5 to set
     */
    public void setNewSize5(Integer newSize5) {
        this.newSize5 = newSize5;
    }

    /**
     * @return the newSize6
     */
    public Integer getNewSize6() {
        return newSize6;
    }

    /**
     * @param newSize6 the newSize6 to set
     */
    public void setNewSize6(Integer newSize6) {
        this.newSize6 = newSize6;
    }

    /**
     * @return the newSize7
     */
    public Integer getNewSize7() {
        return newSize7;
    }

    /**
     * @param newSize7 the newSize7 to set
     */
    public void setNewSize7(Integer newSize7) {
        this.newSize7 = newSize7;
    }

    /**
     * @return the newSize8
     */
    public Integer getNewSize8() {
        return newSize8;
    }

    /**
     * @param newSize8 the newSize8 to set
     */
    public void setNewSize8(Integer newSize8) {
        this.newSize8 = newSize8;
    }

    /**
     * @return the newSize9
     */
    public Integer getNewSize9() {
        return newSize9;
    }

    /**
     * @param newSize9 the newSize9 to set
     */
    public void setNewSize9(Integer newSize9) {
        this.newSize9 = newSize9;
    }

    /**
     * @return the newQuantity
     */
    public Integer getNewQuantity() {
        return newQuantity;
    }

    /**
     * @param newQuantity the newQuantity to set
     */
    public void setNewQuantity(Integer newQuantity) {
        this.newQuantity = newQuantity;
    }

    /**
     * @return the newMrp
     */
    public Double getNewMrp() {
        return newMrp;
    }

    /**
     * @param newMrp the newMrp to set
     */
    public void setNewMrp(Double newMrp) {
        this.newMrp = newMrp;
    }

    /**
     * @return the newTotal
     */
    public Double getNewTotal() {
        return newTotal;
    }

    /**
     * @param newTotal the newTotal to set
     */
    public void setNewTotal(Double newTotal) {
        this.newTotal = newTotal;
    }

    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
}
