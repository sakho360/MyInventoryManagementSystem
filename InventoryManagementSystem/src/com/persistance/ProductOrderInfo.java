package com.persistance;

public class ProductOrderInfo {

    private String pPurchaseOrderNo;
    private String pPurchaseOrderDate;
    private String supplierID;
    private String supplierName;
    private String supplierCompany;
    private String supplierPhone;
    private String supplierFax;
    private String supplierEmail;
    private String supplierZIPCode;
    private String supplierAddress;
    private String receiverName;
    private String receiverCompany;
    private String receiverPhone;
    private String receiverFax;
    private String receivermail;
    private String reciverZIPCode;
    private String receiverAddress;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private Character puachaseOrderStatus;
    private ProductOrderDetailsInfo productOrderDetailsInfo;
    private CompanyInfo companyInfo;

    /**
     * @return the pPurchaseOrderNo
     */
    public String getpPurchaseOrderNo() {
        return pPurchaseOrderNo;
    }

    /**
     * @param pPurchaseOrderNo the pPurchaseOrderNo to set
     */
    public void setpPurchaseOrderNo(String pPurchaseOrderNo) {
        this.pPurchaseOrderNo = pPurchaseOrderNo;
    }

    /**
     * @return the pPurchaseOrderDate
     */
    public String getpPurchaseOrderDate() {
        return pPurchaseOrderDate;
    }

    /**
     * @param pPurchaseOrderDate the pPurchaseOrderDate to set
     */
    public void setpPurchaseOrderDate(String pPurchaseOrderDate) {
        this.pPurchaseOrderDate = pPurchaseOrderDate;
    }

    /**
     * @return the supplierID
     */
    public String getSupplierID() {
        return supplierID;
    }

    /**
     * @param supplierID the supplierID to set
     */
    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    /**
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * @param supplierName the supplierName to set
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * @return the supplierCompany
     */
    public String getSupplierCompany() {
        return supplierCompany;
    }

    /**
     * @param supplierCompany the supplierCompany to set
     */
    public void setSupplierCompany(String supplierCompany) {
        this.supplierCompany = supplierCompany;
    }

    /**
     * @return the supplierPhone
     */
    public String getSupplierPhone() {
        return supplierPhone;
    }

    /**
     * @param supplierPhone the supplierPhone to set
     */
    public void setSupplierPhone(String supplierPhone) {
        this.supplierPhone = supplierPhone;
    }

    /**
     * @return the supplierFax
     */
    public String getSupplierFax() {
        return supplierFax;
    }

    /**
     * @param supplierFax the supplierFax to set
     */
    public void setSupplierFax(String supplierFax) {
        this.supplierFax = supplierFax;
    }

    /**
     * @return the supplierEmail
     */
    public String getSupplierEmail() {
        return supplierEmail;
    }

    /**
     * @param supplierEmail the supplierEmail to set
     */
    public void setSupplierEmail(String supplierEmail) {
        this.supplierEmail = supplierEmail;
    }

    /**
     * @return the supplierZIPCode
     */
    public String getSupplierZIPCode() {
        return supplierZIPCode;
    }

    /**
     * @param supplierZIPCode the supplierZIPCode to set
     */
    public void setSupplierZIPCode(String supplierZIPCode) {
        this.supplierZIPCode = supplierZIPCode;
    }

    /**
     * @return the supplierAddress
     */
    public String getSupplierAddress() {
        return supplierAddress;
    }

    /**
     * @param supplierAddress the supplierAddress to set
     */
    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    /**
     * @return the receiverName
     */
    public String getReceiverName() {
        return receiverName;
    }

    /**
     * @param receiverName the receiverName to set
     */
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    /**
     * @return the receiverCompany
     */
    public String getReceiverCompany() {
        return receiverCompany;
    }

    /**
     * @param receiverCompany the receiverCompany to set
     */
    public void setReceiverCompany(String receiverCompany) {
        this.receiverCompany = receiverCompany;
    }

    /**
     * @return the receiverPhone
     */
    public String getReceiverPhone() {
        return receiverPhone;
    }

    /**
     * @param receiverPhone the receiverPhone to set
     */
    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    /**
     * @return the receiverFax
     */
    public String getReceiverFax() {
        return receiverFax;
    }

    /**
     * @param receiverFax the receiverFax to set
     */
    public void setReceiverFax(String receiverFax) {
        this.receiverFax = receiverFax;
    }

    /**
     * @return the receivermail
     */
    public String getReceivermail() {
        return receivermail;
    }

    /**
     * @param receivermail the receivermail to set
     */
    public void setReceivermail(String receivermail) {
        this.receivermail = receivermail;
    }

    /**
     * @return the reciverZIPCode
     */
    public String getReciverZIPCode() {
        return reciverZIPCode;
    }

    /**
     * @param reciverZIPCode the reciverZIPCode to set
     */
    public void setReciverZIPCode(String reciverZIPCode) {
        this.reciverZIPCode = reciverZIPCode;
    }

    /**
     * @return the receiverAddress
     */
    public String getReceiverAddress() {
        return receiverAddress;
    }

    /**
     * @param receiverAddress the receiverAddress to set
     */
    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the puachaseOrderStatus
     */
    public Character getPuachaseOrderStatus() {
        return puachaseOrderStatus;
    }

    /**
     * @param puachaseOrderStatus the puachaseOrderStatus to set
     */
    public void setPuachaseOrderStatus(Character puachaseOrderStatus) {
        this.puachaseOrderStatus = puachaseOrderStatus;
    }

    /**
     * @return the productOrderDetailsInfo
     */
    public ProductOrderDetailsInfo getProductOrderDetailsInfo() {
        return productOrderDetailsInfo;
    }

    /**
     * @param productOrderDetailsInfo the productOrderDetailsInfo to set
     */
    public void setProductOrderDetailsInfo(ProductOrderDetailsInfo productOrderDetailsInfo) {
        this.productOrderDetailsInfo = productOrderDetailsInfo;
    }

    /**
     * @return the companyInfo
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * @param companyInfo the companyInfo to set
     */
    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }
}
