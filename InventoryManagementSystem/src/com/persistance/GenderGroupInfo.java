package com.persistance;

public class GenderGroupInfo {

    private String genderCode;
    private String genderCodeName;

    /**
     * @return the genderCode
     */
    public String getGenderCode() {
        return genderCode;
    }

    /**
     * @param genderCode the genderCode to set
     */
    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    /**
     * @return the genderCodeName
     */
    public String getGenderCodeName() {
        return genderCodeName;
    }

    /**
     * @param genderCodeName the genderCodeName to set
     */
    public void setGenderCodeName(String genderCodeName) {
        this.genderCodeName = genderCodeName;
    }
}
