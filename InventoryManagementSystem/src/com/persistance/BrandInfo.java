package com.persistance;

public class BrandInfo {

    private String brandCode;
    private String brandName;
    private String brand;
    private String brandDescription;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private Character brandStatus;
    private CompanyInfo companyInfo;

    /**
     * @return the brandCode
     */
    public String getBrandCode() {
        return brandCode;
    }

    /**
     * @param brandCode the brandCode to set
     */
    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    /**
     * @return the brandName
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * @param brandName the brandName to set
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the brandDescription
     */
    public String getBrandDescription() {
        return brandDescription;
    }

    /**
     * @param brandDescription the brandDescription to set
     */
    public void setBrandDescription(String brandDescription) {
        this.brandDescription = brandDescription;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the brandStatus
     */
    public Character getBrandStatus() {
        return brandStatus;
    }

    /**
     * @param brandStatus the brandStatus to set
     */
    public void setBrandStatus(Character brandStatus) {
        this.brandStatus = brandStatus;
    }

    /**
     * @return the companyInfo
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * @param companyInfo the companyInfo to set
     */
    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }
}
