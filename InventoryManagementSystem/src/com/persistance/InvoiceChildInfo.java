package com.persistance;

public class InvoiceChildInfo {

    private Integer invoiceChildID;
    private String itemCode;
    private String productName;
    private String invoiceSize;
    private Integer invoiceSize0;
    private Integer invoiceSize1;
    private Integer invoiceSize2;
    private Integer invoiceSize3;
    private Integer invoiceSize4;
    private Integer invoiceSize5;
    private Integer invoiceSize6;
    private Integer invoiceSize7;
    private Integer invoiceSize8;
    private Integer invoiceSize9;
    private Double invoiceMRP;
    private Integer invoiceQuantity;
    private Double invoiceSubTotal;
    private String description;

    /**
     * @return the invoiceChildID
     */
    public Integer getInvoiceChildID() {
        return invoiceChildID;
    }

    /**
     * @param invoiceChildID the invoiceChildID to set
     */
    public void setInvoiceChildID(Integer invoiceChildID) {
        this.invoiceChildID = invoiceChildID;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the invoiceSize
     */
    public String getInvoiceSize() {
        return invoiceSize;
    }

    /**
     * @param invoiceSize the invoiceSize to set
     */
    public void setInvoiceSize(String invoiceSize) {
        this.invoiceSize = invoiceSize;
    }

    /**
     * @return the invoiceSize0
     */
    public Integer getInvoiceSize0() {
        return invoiceSize0;
    }

    /**
     * @param invoiceSize0 the invoiceSize0 to set
     */
    public void setInvoiceSize0(Integer invoiceSize0) {
        this.invoiceSize0 = invoiceSize0;
    }

    /**
     * @return the invoiceSize1
     */
    public Integer getInvoiceSize1() {
        return invoiceSize1;
    }

    /**
     * @param invoiceSize1 the invoiceSize1 to set
     */
    public void setInvoiceSize1(Integer invoiceSize1) {
        this.invoiceSize1 = invoiceSize1;
    }

    /**
     * @return the invoiceSize2
     */
    public Integer getInvoiceSize2() {
        return invoiceSize2;
    }

    /**
     * @param invoiceSize2 the invoiceSize2 to set
     */
    public void setInvoiceSize2(Integer invoiceSize2) {
        this.invoiceSize2 = invoiceSize2;
    }

    /**
     * @return the invoiceSize3
     */
    public Integer getInvoiceSize3() {
        return invoiceSize3;
    }

    /**
     * @param invoiceSize3 the invoiceSize3 to set
     */
    public void setInvoiceSize3(Integer invoiceSize3) {
        this.invoiceSize3 = invoiceSize3;
    }

    /**
     * @return the invoiceSize4
     */
    public Integer getInvoiceSize4() {
        return invoiceSize4;
    }

    /**
     * @param invoiceSize4 the invoiceSize4 to set
     */
    public void setInvoiceSize4(Integer invoiceSize4) {
        this.invoiceSize4 = invoiceSize4;
    }

    /**
     * @return the invoiceSize5
     */
    public Integer getInvoiceSize5() {
        return invoiceSize5;
    }

    /**
     * @param invoiceSize5 the invoiceSize5 to set
     */
    public void setInvoiceSize5(Integer invoiceSize5) {
        this.invoiceSize5 = invoiceSize5;
    }

    /**
     * @return the invoiceSize6
     */
    public Integer getInvoiceSize6() {
        return invoiceSize6;
    }

    /**
     * @param invoiceSize6 the invoiceSize6 to set
     */
    public void setInvoiceSize6(Integer invoiceSize6) {
        this.invoiceSize6 = invoiceSize6;
    }

    /**
     * @return the invoiceSize7
     */
    public Integer getInvoiceSize7() {
        return invoiceSize7;
    }

    /**
     * @param invoiceSize7 the invoiceSize7 to set
     */
    public void setInvoiceSize7(Integer invoiceSize7) {
        this.invoiceSize7 = invoiceSize7;
    }

    /**
     * @return the invoiceSize8
     */
    public Integer getInvoiceSize8() {
        return invoiceSize8;
    }

    /**
     * @param invoiceSize8 the invoiceSize8 to set
     */
    public void setInvoiceSize8(Integer invoiceSize8) {
        this.invoiceSize8 = invoiceSize8;
    }

    /**
     * @return the invoiceSize9
     */
    public Integer getInvoiceSize9() {
        return invoiceSize9;
    }

    /**
     * @param invoiceSize9 the invoiceSize9 to set
     */
    public void setInvoiceSize9(Integer invoiceSize9) {
        this.invoiceSize9 = invoiceSize9;
    }

    /**
     * @return the invoiceMRP
     */
    public Double getInvoiceMRP() {
        return invoiceMRP;
    }

    /**
     * @param invoiceMRP the invoiceMRP to set
     */
    public void setInvoiceMRP(Double invoiceMRP) {
        this.invoiceMRP = invoiceMRP;
    }

    /**
     * @return the invoiceQuantity
     */
    public Integer getInvoiceQuantity() {
        return invoiceQuantity;
    }

    /**
     * @param invoiceQuantity the invoiceQuantity to set
     */
    public void setInvoiceQuantity(Integer invoiceQuantity) {
        this.invoiceQuantity = invoiceQuantity;
    }

    /**
     * @return the invoiceSubTotal
     */
    public Double getInvoiceSubTotal() {
        return invoiceSubTotal;
    }

    /**
     * @param invoiceSubTotal the invoiceSubTotal to set
     */
    public void setInvoiceSubTotal(Double invoiceSubTotal) {
        this.invoiceSubTotal = invoiceSubTotal;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }
}
