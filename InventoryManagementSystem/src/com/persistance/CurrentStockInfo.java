package com.persistance;

public class CurrentStockInfo {

    private String productStockID;
    private Integer size0;
    private Integer size1;
    private Integer size2;
    private Integer size3;
    private Integer size4;
    private Integer size5;
    private Integer size6;
    private Integer size7;
    private Integer size8;
    private Integer size9;
    private Integer subQuantity;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private Character stockStatus;
    private ProductInfo productInfo;
    private ProductOrderDetailsInfo productOrderDetailsInfo;
    private InvoiceMasterInfo sizeInfo;

    /**
     * @return the productStockID
     */
    public String getProductStockID() {
        return productStockID;
    }

    /**
     * @param productStockID the productStockID to set
     */
    public void setProductStockID(String productStockID) {
        this.productStockID = productStockID;
    }

    /**
     * @return the size0
     */
    public Integer getSize0() {
        return size0;
    }

    /**
     * @param size0 the size0 to set
     */
    public void setSize0(Integer size0) {
        this.size0 = size0;
    }

    /**
     * @return the size1
     */
    public Integer getSize1() {
        return size1;
    }

    /**
     * @param size1 the size1 to set
     */
    public void setSize1(Integer size1) {
        this.size1 = size1;
    }

    /**
     * @return the size2
     */
    public Integer getSize2() {
        return size2;
    }

    /**
     * @param size2 the size2 to set
     */
    public void setSize2(Integer size2) {
        this.size2 = size2;
    }

    /**
     * @return the size3
     */
    public Integer getSize3() {
        return size3;
    }

    /**
     * @param size3 the size3 to set
     */
    public void setSize3(Integer size3) {
        this.size3 = size3;
    }

    /**
     * @return the size4
     */
    public Integer getSize4() {
        return size4;
    }

    /**
     * @param size4 the size4 to set
     */
    public void setSize4(Integer size4) {
        this.size4 = size4;
    }

    /**
     * @return the size5
     */
    public Integer getSize5() {
        return size5;
    }

    /**
     * @param size5 the size5 to set
     */
    public void setSize5(Integer size5) {
        this.size5 = size5;
    }

    /**
     * @return the size6
     */
    public Integer getSize6() {
        return size6;
    }

    /**
     * @param size6 the size6 to set
     */
    public void setSize6(Integer size6) {
        this.size6 = size6;
    }

    /**
     * @return the size7
     */
    public Integer getSize7() {
        return size7;
    }

    /**
     * @param size7 the size7 to set
     */
    public void setSize7(Integer size7) {
        this.size7 = size7;
    }

    /**
     * @return the size8
     */
    public Integer getSize8() {
        return size8;
    }

    /**
     * @param size8 the size8 to set
     */
    public void setSize8(Integer size8) {
        this.size8 = size8;
    }

    /**
     * @return the size9
     */
    public Integer getSize9() {
        return size9;
    }

    /**
     * @param size9 the size9 to set
     */
    public void setSize9(Integer size9) {
        this.size9 = size9;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the stockStatus
     */
    public Character getStockStatus() {
        return stockStatus;
    }

    /**
     * @param stockStatus the stockStatus to set
     */
    public void setStockStatus(Character stockStatus) {
        this.stockStatus = stockStatus;
    }

    /**
     * @return the productInfo
     */
    public ProductInfo getProductInfo() {
        return productInfo;
    }

    /**
     * @param productInfo the productInfo to set
     */
    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    /**
     * @return the productOrderDetailsInfo
     */
    public ProductOrderDetailsInfo getProductOrderDetailsInfo() {
        return productOrderDetailsInfo;
    }

    /**
     * @param productOrderDetailsInfo the productOrderDetailsInfo to set
     */
    public void setProductOrderDetailsInfo(ProductOrderDetailsInfo productOrderDetailsInfo) {
        this.productOrderDetailsInfo = productOrderDetailsInfo;
    }

    /**
     * @return the sizeInfo
     */
    public InvoiceMasterInfo getSizeInfo() {
        return sizeInfo;
    }

    /**
     * @param sizeInfo the sizeInfo to set
     */
    public void setSizeInfo(InvoiceMasterInfo sizeInfo) {
        this.sizeInfo = sizeInfo;
    }

    /**
     * @return the subQuantity
     */
    public Integer getSubQuantity() {
        return subQuantity;
    }

    /**
     * @param subQuantity the subQuantity to set
     */
    public void setSubQuantity(Integer subQuantity) {
        this.subQuantity = subQuantity;
    }
}
