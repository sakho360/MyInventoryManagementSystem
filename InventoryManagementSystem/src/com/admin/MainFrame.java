package com.admin;

import com.customer.CustomerList;
import com.customer.CustomerSignIn;
import com.profile.ChangePassword;
import com.profile.CompanyProfile;
import product.order.ProductPurchaseOrder;
import com.settings.ProductCategory;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import product.item.ProductItem;
import com.settings.ProductBrand;
import com.settings.ProductSize;
import com.settings.ProductSubCategory;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import product.adjustment.ProductManuallyAdjustment;
import product.adjustment.ProductExchange;
import product.adjustment.ProductExchangeList;
import product.adjustment.ProductReturn;
import product.adjustment.ProductReturnList;
import product.item.ProductItemHistory;
import product.order.ProductPurchaseOrderHistory;
import product.receive.ProductReceive;
import product.receive.ProductReceiveHistory;
import product.invoice.ProductInvoiceList;
import product.invoice.ProductInvoice;

public class MainFrame extends javax.swing.JFrame {

    private Login login = null;
    private HashMap session = null;

    public MainFrame(HashMap s) {

        this.session = s;
        initComponents();

        setComponent();

        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets scnMax = Toolkit.getDefaultToolkit().getScreenInsets(getGraphicsConfiguration());
        int taskBarSize = scnMax.bottom;
        pack();
        setSize(screenSize.width, screenSize.height - taskBarSize);
    }

    public void setComponent() {
        try {
            setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Images/inven.png")));
            setTitle("Inventory Management System");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Images/logo5.jpg"));
        Image image = imageIcon.getImage();
        jdpMain = new javax.swing.JDesktopPane(){
            public void paintComponent(Graphics g){
                g.drawImage(image, 0, 0, this);
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jmPEntry = new javax.swing.JMenu();
        jmiProductEntry = new javax.swing.JMenuItem();
        jmiProductHistory = new javax.swing.JMenuItem();
        jmPPurchase = new javax.swing.JMenu();
        jmiProductPurchaseEntry = new javax.swing.JMenuItem();
        jmiProductPurchaseOrderList = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jmiOrderReceive = new javax.swing.JMenuItem();
        jmiOrderReceiveHistory = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jmiProductSale = new javax.swing.JMenuItem();
        jmiProductSaleList = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jmiCreateCustomerID = new javax.swing.JMenuItem();
        jmiCustomerList = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jmiProductReturn = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jmiProductExchange = new javax.swing.JMenuItem();
        jmiProductExchangeList = new javax.swing.JMenuItem();
        jmiProductAdjustment = new javax.swing.JMenuItem();
        jmSettings = new javax.swing.JMenu();
        jmiPSize = new javax.swing.JMenuItem();
        jmiPBrand = new javax.swing.JMenuItem();
        jmiPCategory = new javax.swing.JMenuItem();
        jmiPSubCategory = new javax.swing.JMenuItem();
        jmiProfile = new javax.swing.JMenuItem();
        jmiChPass = new javax.swing.JMenuItem();
        jmiLogout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jdpMain.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jdpMainLayout = new javax.swing.GroupLayout(jdpMain);
        jdpMain.setLayout(jdpMainLayout);
        jdpMainLayout.setHorizontalGroup(
            jdpMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 900, Short.MAX_VALUE)
        );
        jdpMainLayout.setVerticalGroup(
            jdpMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 481, Short.MAX_VALUE)
        );

        jmPEntry.setText("Product Entry");
        jmPEntry.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmPEntry.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jmPEntryMouseEntered(evt);
            }
        });

        jmiProductEntry.setText("Product Entry");
        jmiProductEntry.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductEntryActionPerformed(evt);
            }
        });
        jmPEntry.add(jmiProductEntry);

        jmiProductHistory.setText("Product List");
        jmiProductHistory.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductHistoryActionPerformed(evt);
            }
        });
        jmPEntry.add(jmiProductHistory);

        jMenuBar1.add(jmPEntry);

        jmPPurchase.setText("Product Order");
        jmPPurchase.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jmiProductPurchaseEntry.setText("Product Order");
        jmiProductPurchaseEntry.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductPurchaseEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductPurchaseEntryActionPerformed(evt);
            }
        });
        jmPPurchase.add(jmiProductPurchaseEntry);

        jmiProductPurchaseOrderList.setText("Product Order List");
        jmiProductPurchaseOrderList.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductPurchaseOrderList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductPurchaseOrderListActionPerformed(evt);
            }
        });
        jmPPurchase.add(jmiProductPurchaseOrderList);

        jMenuBar1.add(jmPPurchase);

        jMenu1.setText("Product Receive");
        jMenu1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jmiOrderReceive.setText("Product  Receive");
        jmiOrderReceive.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiOrderReceive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiOrderReceiveActionPerformed(evt);
            }
        });
        jMenu1.add(jmiOrderReceive);

        jmiOrderReceiveHistory.setText("Product Receive List");
        jmiOrderReceiveHistory.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiOrderReceiveHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiOrderReceiveHistoryActionPerformed(evt);
            }
        });
        jMenu1.add(jmiOrderReceiveHistory);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Product Invoice");
        jMenu2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jmiProductSale.setText("Product Invoice");
        jmiProductSale.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductSale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductSaleActionPerformed(evt);
            }
        });
        jMenu2.add(jmiProductSale);

        jmiProductSaleList.setText("Product Invoice List");
        jmiProductSaleList.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductSaleList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductSaleListActionPerformed(evt);
            }
        });
        jMenu2.add(jmiProductSaleList);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Customer");
        jMenu3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jmiCreateCustomerID.setText("Create Customer ID");
        jmiCreateCustomerID.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiCreateCustomerID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiCreateCustomerIDActionPerformed(evt);
            }
        });
        jMenu3.add(jmiCreateCustomerID);

        jmiCustomerList.setText("Customer List");
        jmiCustomerList.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiCustomerList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiCustomerListActionPerformed(evt);
            }
        });
        jMenu3.add(jmiCustomerList);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Product Adjustment");
        jMenu4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jmiProductReturn.setText("Product Return");
        jmiProductReturn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductReturnActionPerformed(evt);
            }
        });
        jMenu4.add(jmiProductReturn);

        jMenuItem1.setText("Product Return List");
        jMenuItem1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem1);

        jmiProductExchange.setText("Product Exchange");
        jmiProductExchange.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductExchange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductExchangeActionPerformed(evt);
            }
        });
        jMenu4.add(jmiProductExchange);

        jmiProductExchangeList.setText("Product Exchange List");
        jmiProductExchangeList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductExchangeListActionPerformed(evt);
            }
        });
        jMenu4.add(jmiProductExchangeList);

        jmiProductAdjustment.setText("Manual Adjustment");
        jmiProductAdjustment.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProductAdjustment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductAdjustmentActionPerformed(evt);
            }
        });
        jMenu4.add(jmiProductAdjustment);

        jMenuBar1.add(jMenu4);

        jmSettings.setText("Settings");
        jmSettings.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jmiPSize.setText("Size");
        jmiPSize.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiPSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiPSizeActionPerformed(evt);
            }
        });
        jmSettings.add(jmiPSize);

        jmiPBrand.setText("Brand");
        jmiPBrand.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiPBrand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiPBrandActionPerformed(evt);
            }
        });
        jmSettings.add(jmiPBrand);

        jmiPCategory.setText("Category");
        jmiPCategory.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiPCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiPCategoryActionPerformed(evt);
            }
        });
        jmSettings.add(jmiPCategory);

        jmiPSubCategory.setText("Sub Category");
        jmiPSubCategory.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiPSubCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiPSubCategoryActionPerformed(evt);
            }
        });
        jmSettings.add(jmiPSubCategory);

        jmiProfile.setText("Company Profile");
        jmiProfile.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProfileActionPerformed(evt);
            }
        });
        jmSettings.add(jmiProfile);

        jmiChPass.setText("Change Password");
        jmiChPass.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiChPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiChPassActionPerformed(evt);
            }
        });
        jmSettings.add(jmiChPass);

        jmiLogout.setText("Logout");
        jmiLogout.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiLogoutActionPerformed(evt);
            }
        });
        jmSettings.add(jmiLogout);

        jMenuBar1.add(jmSettings);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jdpMain)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jdpMain)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jmiProductPurchaseEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductPurchaseEntryActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductPurchaseOrder productPurchaseOrder = new ProductPurchaseOrder(session);
        jdpMain.add(productPurchaseOrder);
        productPurchaseOrder.show();
        productPurchaseOrder.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductPurchaseEntryActionPerformed

    private void jmiProductEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductEntryActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductItem productEntry = new ProductItem(session);
        jdpMain.add(productEntry);
        productEntry.setVisible(true);
        productEntry.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductEntryActionPerformed

    private void jmiLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiLogoutActionPerformed

        this.setVisible(false);
        login = new Login();
        login.setVisible(true);
    }//GEN-LAST:event_jmiLogoutActionPerformed

    private void jmiChPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiChPassActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ChangePassword changePassword = new ChangePassword(session);
        jdpMain.add(changePassword);
        changePassword.show();
        changePassword.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiChPassActionPerformed

    private void jmiProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProfileActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        CompanyProfile companyProfile = new CompanyProfile();
        jdpMain.add(companyProfile);
        companyProfile.show();
        companyProfile.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProfileActionPerformed

    private void jmiPSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiPSizeActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductSize productPurchaseSize = new ProductSize(session);
        jdpMain.add(productPurchaseSize);
        productPurchaseSize.show();
        productPurchaseSize.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiPSizeActionPerformed

    private void jmiPBrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiPBrandActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductBrand productPurchaseBrand = new ProductBrand(session);
        jdpMain.add(productPurchaseBrand);
        productPurchaseBrand.show();
        productPurchaseBrand.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiPBrandActionPerformed

    private void jmiPCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiPCategoryActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductCategory category = new ProductCategory(session);
        jdpMain.add(category);
        category.show();
        category.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiPCategoryActionPerformed

    private void jmiPSubCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiPSubCategoryActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductSubCategory subCategory = new ProductSubCategory(session);
        jdpMain.add(subCategory);
        subCategory.show();
        subCategory.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiPSubCategoryActionPerformed

    private void jmiProductHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductHistoryActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductItemHistory itemHistory = new ProductItemHistory(session);
        jdpMain.add(itemHistory);
        itemHistory.show();
        itemHistory.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductHistoryActionPerformed

    private void jmiProductPurchaseOrderListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductPurchaseOrderListActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductPurchaseOrderHistory productPurchaseOrderHistory = new ProductPurchaseOrderHistory(session);
        jdpMain.add(productPurchaseOrderHistory);
        productPurchaseOrderHistory.show();
        productPurchaseOrderHistory.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductPurchaseOrderListActionPerformed

    private void jmiOrderReceiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiOrderReceiveActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductReceive productReceive = new ProductReceive(session);
        jdpMain.add(productReceive);
        productReceive.show();
        productReceive.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiOrderReceiveActionPerformed

    private void jmiOrderReceiveHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiOrderReceiveHistoryActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductReceiveHistory productReceiveHistory = new ProductReceiveHistory(session);
        jdpMain.add(productReceiveHistory);
        productReceiveHistory.show();
        productReceiveHistory.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiOrderReceiveHistoryActionPerformed

    private void jmiProductSaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductSaleActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductInvoice productSale = new ProductInvoice(session);
        jdpMain.add(productSale);
        productSale.show();
        productSale.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductSaleActionPerformed

    private void jmiProductSaleListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductSaleListActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductInvoiceList productInvoiceList = new ProductInvoiceList(session);
        jdpMain.add(productInvoiceList);
        productInvoiceList.show();
        productInvoiceList.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductSaleListActionPerformed

    private void jmiCustomerListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiCustomerListActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        CustomerList customerList = new CustomerList(session);
        jdpMain.add(customerList);
        customerList.show();
        customerList.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiCustomerListActionPerformed

    private void jmiCreateCustomerIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiCreateCustomerIDActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        CustomerSignIn customerSignIn = new CustomerSignIn(session);
        jdpMain.add(customerSignIn);
        customerSignIn.show();
        customerSignIn.setLocation((jdpMain.getSize().width - customerSignIn.getSize().width) / 2, (jdpMain.getSize().height - customerSignIn.getSize().height) / 2);
//        customerSignIn.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiCreateCustomerIDActionPerformed

    private void jmiProductReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductReturnActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductReturn productReturn = new ProductReturn(session);
        jdpMain.add(productReturn);
        productReturn.show();
        productReturn.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductReturnActionPerformed

    private void jmiProductExchangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductExchangeActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductExchange productExchange = new ProductExchange(session);
        jdpMain.add(productExchange);
        productExchange.show();
        productExchange.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductExchangeActionPerformed

    private void jmiProductAdjustmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductAdjustmentActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductManuallyAdjustment productAdjustment = new ProductManuallyAdjustment(session);
        jdpMain.add(productAdjustment);
        productAdjustment.show();
        productAdjustment.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductAdjustmentActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductReturnList productReturnList = new ProductReturnList(session);
        jdpMain.add(productReturnList);
        productReturnList.show();
        productReturnList.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jmPEntryMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jmPEntryMouseEntered
        jmPEntry.setBackground(Color.RED);
    }//GEN-LAST:event_jmPEntryMouseEntered

    private void jmiProductExchangeListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductExchangeListActionPerformed

        jdpMain.removeAll();
        jdpMain.updateUI();

        ProductExchangeList productExchangeList = new ProductExchangeList(session);
        jdpMain.add(productExchangeList);
        productExchangeList.show();
        productExchangeList.setSize(jdpMain.getSize());
    }//GEN-LAST:event_jmiProductExchangeListActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JDesktopPane jdpMain;
    private javax.swing.JMenu jmPEntry;
    private javax.swing.JMenu jmPPurchase;
    private javax.swing.JMenu jmSettings;
    private javax.swing.JMenuItem jmiChPass;
    private javax.swing.JMenuItem jmiCreateCustomerID;
    private javax.swing.JMenuItem jmiCustomerList;
    private javax.swing.JMenuItem jmiLogout;
    private javax.swing.JMenuItem jmiOrderReceive;
    private javax.swing.JMenuItem jmiOrderReceiveHistory;
    private javax.swing.JMenuItem jmiPBrand;
    private javax.swing.JMenuItem jmiPCategory;
    private javax.swing.JMenuItem jmiPSize;
    private javax.swing.JMenuItem jmiPSubCategory;
    private javax.swing.JMenuItem jmiProductAdjustment;
    private javax.swing.JMenuItem jmiProductEntry;
    private javax.swing.JMenuItem jmiProductExchange;
    private javax.swing.JMenuItem jmiProductExchangeList;
    private javax.swing.JMenuItem jmiProductHistory;
    private javax.swing.JMenuItem jmiProductPurchaseEntry;
    private javax.swing.JMenuItem jmiProductPurchaseOrderList;
    private javax.swing.JMenuItem jmiProductReturn;
    private javax.swing.JMenuItem jmiProductSale;
    private javax.swing.JMenuItem jmiProductSaleList;
    private javax.swing.JMenuItem jmiProfile;
    // End of variables declaration//GEN-END:variables
}
