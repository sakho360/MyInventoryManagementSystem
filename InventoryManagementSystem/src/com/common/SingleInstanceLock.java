package com.common;

import java.io.File;
import java.io.IOException;

public class SingleInstanceLock {

    private static final String LOCK_FILEPATH = System.getProperty("java.io.tmpdir") + File.separator + "lector.lock";
    private static final File lock = new File(LOCK_FILEPATH);
    private static boolean locked = false;

    private SingleInstanceLock() {
    }

    public static boolean lock() throws IOException {

        if (locked) {
            System.out.println(LOCK_FILEPATH);
            return true;
        }

        if (lock.exists()) {
            System.out.println(LOCK_FILEPATH);
            return false;
        }

        lock.createNewFile();
        lock.deleteOnExit();
        locked = true;

        return true;
    }

    public static void main(String[] args) throws IOException {
        lock.deleteOnExit();
    }
}
