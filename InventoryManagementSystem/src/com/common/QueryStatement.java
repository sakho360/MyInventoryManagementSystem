package com.common;

public class QueryStatement {

    public static String selectLoginInfo(String email, String password) {

        String selectLoginStatement = " SELECT "
                + " UI.user_id, "
                + " UI.first_name, "
                + " UI.last_name, "
                + " UI.user_email, "
                + " UI.user_password, "
                + " UI.user_phone, "
                + " UI.user_country, "
                + " UI.active_status, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.update_by, "
                + " UI.update_date, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.suspend_status "
                + " FROM "
                + " user_info UI "
                + " WHERE "
                + " UI.user_email = '" + email + "' "
                + " AND "
                + " UI.user_password = '" + password + "' "
                + " AND "
                + " UI.suspend_status = 'Y' ";

        return selectLoginStatement;
    }

    public static String updatePasswordQuery(String email, String pass, String updateBy) {

        String userPasswordUpdateStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " user_password = '" + pass + "' , "
                + " update_by = '" + updateBy + "' , "
                + " update_date = CURRENT_TIMESTAMP "
                + " WHERE "
                + " user_email = '" + email + "' ";

        return userPasswordUpdateStatement;
    }

    public static String selectSizeInfo() {

        String selectSizeInfoStatement = " SELECT "
                + " size_code, "
                + " size_unit, "
                + " size0, "
                + " size1, "
                + " size2, "
                + " size3, "
                + " size4, "
                + " size5, "
                + " size6, "
                + " size7, "
                + " size8, "
                + " size9, "
                + " size_desc, "
                + " insert_by, "
                + " insert_date, "
                + " update_by, "
                + " update_date, "
                + " size_status, "
                + " company_id "
                + " FROM "
                + " size_info";

        return selectSizeInfoStatement;
    }

    public static String selectBrandInfo() {

        String selectBrandInfoStatement = " SELECT "
                + " brand_code, "
                + " brand_name, "
                + " brand_description, "
                + " company_id, "
                + " insert_by, "
                + " insert_date, "
                + " update_by, "
                + " update_date, "
                + " brand_status "
                + " FROM "
                + " brand_info";

        return selectBrandInfoStatement;
    }

    public static String selectCategoryInfo() {

        String selectCategoryInfoStatement = " SELECT "
                + " category_code, "
                + " category_name, "
                + " category_description, "
                + " company_id, "
                + " insert_by, "
                + " insert_date, "
                + " update_by, "
                + " update_date, "
                + " category_status "
                + " FROM "
                + " category_info";

        return selectCategoryInfoStatement;
    }

    public static String selectSubCategoryInfo() {

        String selectSubCategoryInfoStatement = " SELECT "
                + " SCI.sub_category_code, "
                + " SCI.sub_category_name, "
                + " SCI.sub_category_description, "
                + " SCI.sub_category_status, "
                + " SCI.insert_by, "
                + " SCI.insert_date, "
                + " SCI.update_by, "
                + " SCI.update_date, "
                + " CI.category_code, "
                + " CI.category_name, "
                + " COM.com_id, "
                + " COM.com_name "
                + " FROM "
                + " sub_category_info SCI, "
                + " category_info CI, "
                + " company_info COM "
                + " WHERE "
                + " SCI.category_code = CI.category_code "
                + " AND "
                + " SCI.company_id = COM.com_id ";

        return selectSubCategoryInfoStatement;
    }

    public static String selectProductInfo() {

        String selectProductInfoStatement = " SELECT "
                + " PI.pro_item_code, "
                + " PI.pro_cost_price, "
                + " PI.pro_vat, "
                + " PI.pro_net_price, "
                + " PI.pro_mrp, "
                + " PI.pro_supplier, "
                + " PI.pro_supp_reference, "
                + " PI.pro_state, "
                + " PI.pro_temp, "
                + " PI.pro_life_cycle, "
                + " PI.pro_image_url, "
                + " PI.pro_desc, "
                + " PI.pro_status, "
                + " SI.size_code, "
                + " SI.size_unit, "
                + " BI.brand_code, "
                + " BI.brand_name, "
                + " CI.category_code, "
                + " CI.category_name, "
                + " SCI.sub_category_code, "
                + " SCI.sub_category_name, "
                + " COM.com_id, "
                + " COM.com_name, "
                + " COLI.color_code, "
                + " COLI.color_name, "
                + " GGI.gender_group_code, "
                + " GGI.gender_group_name, "
                + " AGI.age_group_code, "
                + " AGI.age_group_name "
                + " FROM "
                + " product_info PI, "
                + " size_info SI, "
                + " brand_info BI, "
                + " category_info CI, "
                + " sub_category_info SCI, "
                + " company_info COM, "
                + " color_info COLI, "
                + " gender_group_info GGI, "
                + " age_group_info AGI "
                + " WHERE "
                + " PI.size_code = SI.size_code "
                + " AND "
                + " PI.brand_code = BI.brand_code "
                + " AND "
                + " PI.category_code = CI.category_code "
                + " AND "
                + " PI.sub_category_code = SCI.sub_category_code "
                + " AND "
                + " PI.company_id = COM.com_id "
                + " AND "
                + " PI.color_code = COLI.color_code "
                + " AND "
                + " PI.gender_group_code = GGI.gender_group_code "
                + " AND "
                + " PI.age_group_code = AGI.age_group_code ";

        return selectProductInfoStatement;
    }

    public static String searchProductInfo(String fieldName, String fieldValue) {

        String searchProductInfoStatement = "";

        if (fieldName.equals("itemCode")) {

            searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                    + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                    + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                    + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                    + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                    + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                    + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                    + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                    + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                    + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                    + " PI.age_group_code = AGI.age_group_code AND PI.pro_item_code = '" + fieldValue + "' ";
        } else if (fieldName.equals("cName")) {

            searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                    + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                    + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                    + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                    + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                    + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                    + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                    + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                    + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                    + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                    + " PI.age_group_code = AGI.age_group_code AND CI.category_name = '" + fieldValue + "' ";
        } else if (fieldName.equals("scName")) {

            searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                    + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                    + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                    + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                    + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                    + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                    + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                    + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                    + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                    + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                    + " PI.age_group_code = AGI.age_group_code AND SCI.sub_category_name = '" + fieldValue + "' ";
        } else if (fieldName.equals("bName")) {

            searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                    + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                    + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                    + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                    + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                    + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                    + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                    + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                    + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                    + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                    + " PI.age_group_code = AGI.age_group_code AND BI.brand_name = '" + fieldValue + "' ";
        } else if (fieldName.equals("size")) {

            searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                    + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                    + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                    + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                    + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                    + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                    + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                    + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                    + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                    + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                    + " PI.age_group_code = AGI.age_group_code AND SI.size_unit = '" + fieldValue + "' ";
        } else if (fieldName.equals("color")) {

            searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                    + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                    + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                    + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                    + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                    + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                    + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                    + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                    + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                    + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                    + " PI.age_group_code = AGI.age_group_code AND COLI.color_name = '" + fieldValue + "' ";
        } else if (fieldName.equals("age")) {

            searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                    + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                    + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                    + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                    + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                    + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                    + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                    + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                    + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                    + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                    + " PI.age_group_code = AGI.age_group_code AND AGI.age_group_name = '" + fieldValue + "' ";
        } else if (fieldName.equals("gender")) {

            searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                    + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                    + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                    + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                    + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                    + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                    + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                    + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                    + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                    + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                    + " PI.age_group_code = AGI.age_group_code AND GGI.gender_group_name = '" + fieldValue + "' ";
        } else if (fieldName.equals("mrp")) {

            String[] mrpArray = null;
            Double mrpFrom = 0d;
            Double mrpTo = 0d;

            if (!fieldValue.isEmpty()) {

                mrpArray = fieldValue.split(",");

                if (mrpArray.length > 0) {
                    for (int i = 0; i < mrpArray.length; i++) {
                        mrpFrom = Double.parseDouble(mrpArray[0]);
                        mrpTo = Double.parseDouble(mrpArray[1]);
                    }
                }
            }

            if (mrpFrom > mrpTo) {

                searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                        + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                        + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                        + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                        + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                        + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                        + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                        + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                        + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                        + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                        + " PI.age_group_code = AGI.age_group_code AND PI.pro_mrp BETWEEN '" + mrpTo + "' AND '" + mrpFrom + "' ";
            } else {

                searchProductInfoStatement = " SELECT PI.pro_item_code, PI.pro_cost_price, PI.pro_vat, "
                        + " PI.pro_net_price, PI.pro_mrp, PI.pro_supplier, PI.pro_supp_reference, PI.pro_state, PI.pro_temp, "
                        + " PI.pro_life_cycle, PI.pro_desc, PI.pro_image_url, PI.pro_status, SI.size_code, SI.size_unit, BI.brand_code, BI.brand_name, "
                        + " CI.category_code, CI.category_name, SCI.sub_category_code, SCI.sub_category_name, COM.com_id, COM.com_name, "
                        + " COLI.color_code, COLI.color_name, GGI.gender_group_code, GGI.gender_group_name, AGI.age_group_code, "
                        + " AGI.age_group_name FROM product_info PI, size_info SI, brand_info BI, category_info CI, "
                        + " sub_category_info SCI, company_info COM, color_info COLI, gender_group_info GGI, age_group_info AGI WHERE "
                        + " PI.size_code = SI.size_code  AND PI.brand_code = BI.brand_code AND PI.category_code = CI.category_code "
                        + " AND PI.sub_category_code = SCI.sub_category_code AND PI.company_id = COM.com_id AND "
                        + " PI.color_code = COLI.color_code AND PI.gender_group_code = GGI.gender_group_code AND "
                        + " PI.age_group_code = AGI.age_group_code AND PI.pro_mrp BETWEEN '" + mrpFrom + "' AND '" + mrpTo + "' ";
            }
        }

        return searchProductInfoStatement;
    }

    public static String selectProductOrderInfo() {

        String selectProductOrderInfoStatement = " SELECT "
                + " POI.product_order_no, "
                + " POI.purchase_order_date, "
                + " POI.supplier_id, "
                + " POI.supplier_name, "
                + " POI.supplier_company, "
                + " POI.supplier_phone, "
                + " POI.supplier_fax, "
                + " POI.supplier_email, "
                + " POI.supplier_zip_code, "
                + " POI.supplier_address, "
                + " POI.puachase_order_status, "
                + " POD.item_code, "
                + " POD.size_code, "
                + " POD.net_price, "
                + " POD.quantity, "
                + " POD.s0, "
                + " POD.s1, "
                + " POD.s2, "
                + " POD.s3, "
                + " POD.s4, "
                + " POD.s5, "
                + " POD.s6, "
                + " POD.s7, "
                + " POD.s8, "
                + " POD.s9, "
                + " POD.sub_total, "
                + " POD.product_order_no, "
                + " CI.com_id, "
                + " CI.com_name "
                + " FROM "
                + " product_order_info POI, "
                + " product_order_details POD, "
                + " company_info CI "
                + " WHERE "
                + " POI.product_order_no = POD.product_order_no "
                + " AND "
                + " POI.company_id = CI.com_id ";

        return selectProductOrderInfoStatement;
    }

    public static String selectProductSizeInfo(String itemCode) {

        String selectProductSizeInfoStatement = " SELECT "
                + " PI.pro_item_code, "
                + " PI.pro_cost_price, "
                + " PI.pro_vat, "
                + " PI.pro_net_price, "
                + " PI.pro_mrp, "
                + " SI.size_code, "
                + " SI.size_unit, "
                + " SI.size0, "
                + " SI.size1, "
                + " SI.size2, "
                + " SI.size3, "
                + " SI.size4, "
                + " SI.size5, "
                + " SI.size6, "
                + " SI.size7, "
                + " SI.size8, "
                + " SI.size9 "
                + " FROM "
                + " product_info PI, "
                + " size_info SI "
                + " WHERE "
                + " PI.size_code = SI.size_code "
                + " AND "
                + " PI.pro_item_code = '" + itemCode + "' ";

        return selectProductSizeInfoStatement;
    }

    public static String selectProductOrderInfoBySearch(String orderNo) {

        String selectProductOrderInfoStatement = " SELECT "
                + " POI.product_order_no, "
                + " POI.purchase_order_date, "
                + " POI.supplier_id, "
                + " POI.supplier_name, "
                + " POI.supplier_company, "
                + " POI.supplier_phone, "
                + " POI.supplier_fax, "
                + " POI.supplier_email, "
                + " POI.supplier_zip_code, "
                + " POI.supplier_address, "
                + " POI.puachase_order_status, "
                + " POD.item_code, "
                + " POD.size_code, "
                + " POD.net_price, "
                + " POD.quantity, "
                + " POD.s0, "
                + " POD.s1, "
                + " POD.s2, "
                + " POD.s3, "
                + " POD.s4, "
                + " POD.s5, "
                + " POD.s6, "
                + " POD.s7, "
                + " POD.s8, "
                + " POD.s9, "
                + " POD.sub_total, "
                + " POD.order_status, "
                + " POD.product_order_no, "
                + " CI.com_id, "
                + " CI.com_name "
                + " FROM "
                + " product_order_info POI, "
                + " product_order_details POD, "
                + " company_info CI "
                + " WHERE "
                + " POI.product_order_no = POD.product_order_no "
                + " AND "
                + " POI.company_id = CI.com_id "
                + " AND "
                + " POI.product_order_no = '" + orderNo + "' ";

        return selectProductOrderInfoStatement;
    }

    public static String selectProductOrderReceiveInfoBySearch(String orderNo) {

        String selectProductOrderReceiveInfoStatement = " SELECT "
                + " PORI.order_receive_id, "
                + " PORI.order_no, "
                + " PORI.order_date, "
                + " PORI.item_code, "
                + " PORI.size_code, "
                + " PORI.s0_received, "
                + " PORI.s1_received, "
                + " PORI.s2_received, "
                + " PORI.s3_received, "
                + " PORI.s4_received, "
                + " PORI.s5_received, "
                + " PORI.s6_received, "
                + " PORI.s7_received, "
                + " PORI.s8_received, "
                + " PORI.s9_received, "
                + " PORI.receive_mrp, "
                + " PORI.receive_quantity, "
                + " PORI.receive_sub_total, "
                + " PORI.receive_status, "
                + " PORI.received_by, "
                + " PORI.receive_date "
                + " FROM "
                + " order_receive_info PORI "
                + " WHERE "
                + " PORI.order_no = '" + orderNo + "' ";

        return selectProductOrderReceiveInfoStatement;
    }

    public static String selectProductOrderReceiveInfo() {

        String selectProductOrderReceiveInfoStatement = " SELECT "
                + " PORI.order_receive_id, "
                + " PORI.order_no, "
                + " PORI.order_date, "
                + " PORI.item_code, "
                + " PORI.size_code, "
                + " PORI.s0_received, "
                + " PORI.s1_received, "
                + " PORI.s2_received, "
                + " PORI.s3_received, "
                + " PORI.s4_received, "
                + " PORI.s5_received, "
                + " PORI.s6_received, "
                + " PORI.s7_received, "
                + " PORI.s8_received, "
                + " PORI.s9_received, "
                + " PORI.receive_mrp, "
                + " PORI.receive_quantity, "
                + " PORI.receive_sub_total, "
                + " PORI.receive_status, "
                + " PORI.received_by, "
                + " PORI.receive_date "
                + " FROM "
                + " order_receive_info PORI ";

        return selectProductOrderReceiveInfoStatement;
    }

    public static String selectCustomerInfoBySearch(String cusID) {

        String selectCustomerInfoStatement = " SELECT "
                + " CI.customer_id, "
                + " CI.full_name, "
                + " CI.contact_no, "
                + " CI.dob, "
                + " CI.religion, "
                + " CI.gender, "
                + " CI.hot_line, "
                + " CI.address, "
                + " CI.status "
                + " FROM "
                + " customer_info CI "
                + " WHERE "
                + " CI.customer_id = '" + cusID + "' ";

        return selectCustomerInfoStatement;
    }

    public static String selectProductDetailsForInvoice(String itemCode) {

        String selectProductDetailsInfoStatement = " SELECT "
                + " PI.pro_item_code, "
                + " PI.pro_name, "
                + " PI.pro_cost_price, "
                + " PI.pro_vat, "
                + " PI.pro_net_price, "
                + " PI.pro_mrp, "
                + " PI.pro_supplier, "
                + " PI.pro_supp_reference, "
                + " PI.pro_state, "
                + " PI.pro_temp, "
                + " PI.pro_life_cycle, "
                + " PI.pro_image_url, "
                + " PI.pro_desc, "
                + " PI.pro_status, "
                + " SI.size_code, "
                + " SI.size_unit, "
                + " SI.size0, "
                + " SI.size1, "
                + " SI.size2, "
                + " SI.size3, "
                + " SI.size4, "
                + " SI.size5, "
                + " SI.size6, "
                + " SI.size7, "
                + " SI.size8, "
                + " SI.size9, "
                + " BI.brand_code, "
                + " BI.brand_name, "
                + " CI.category_code, "
                + " CI.category_name, "
                + " SCI.sub_category_code, "
                + " SCI.sub_category_name, "
                + " POD.item_code, "
                + " POD.order_date, "
                + " CSI.item_code, "
                + " CSI.order_no, "
                + " CSI.size_code, "
                + " CSI.stock_size0, "
                + " CSI.stock_size1, "
                + " CSI.stock_size2, "
                + " CSI.stock_size3, "
                + " CSI.stock_size4, "
                + " CSI.stock_size5, "
                + " CSI.stock_size6, "
                + " CSI.stock_size7, "
                + " CSI.stock_size8, "
                + " CSI.stock_size9, "
                + " CSI.stock_sub_quantity, "
                + " COLOR.color_code, "
                + " COLOR.color_name, "
                + " AGE.age_group_code, "
                + " AGE.age_group_name, "
                + " GEND.gender_group_code, "
                + " GEND.gender_group_name "
                + " FROM "
                + " product_info PI, "
                + " size_info SI, "
                + " brand_info BI, "
                + " category_info CI, "
                + " sub_category_info SCI, "
                + " product_order_details POD, "
                + " current_stock_info CSI, "
                + " color_info COLOR, "
                + " age_group_info AGE, "
                + " gender_group_info GEND "
                + " WHERE "
                + " PI.size_code = SI.size_code "
                + " AND "
                + " PI.brand_code = BI.brand_code "
                + " AND "
                + " PI.category_code = CI.category_code "
                + " AND "
                + " PI.sub_category_code = SCI.sub_category_code "
                + " AND "
                + " PI.pro_item_code = POD.item_code "
                + " AND "
                + " PI.pro_item_code = CSI.item_code "
                + " AND "
                + " PI.color_code = COLOR.color_code "
                + " AND "
                + " PI.age_group_code = AGE.age_group_code "
                + " AND "
                + " PI.gender_group_code = GEND.gender_group_code "
                + " AND "
                + " PI.pro_item_code = '" + itemCode + "' ";

        return selectProductDetailsInfoStatement;
    }

    public static String selectProductInvoiceInfo() {

        String selectProductInvoiceInfoStatement = " SELECT "
                + " IVM.invoice_no, "
                + " IVM.customer_id, "
                + " IVM.customer_name, "
                + " IVM.customer_mobile, "
                + " IVM.customer_address, "
                + " IVM.invoice_status, "
                + " IVM.grand_total, "
                + " IVM.discount, "
                + " IVM.advance, "
                + " IVM.delivery_charge, "
                + " IVM.invoice_by, "
                + " IVM.invoice_date, "
                + " IVC.item_code, "
                + " IVC.product_name, "
                + " IVC.invoice_quantity, "
                + " IVC.invoice_mrp, "
                + " IVC.invoice_sub_total, "
                + " IVC.description "
                + " FROM "
                + " invoice_master IVM, "
                + " invoice_child IVC "
                + " WHERE "
                + " IVM.invoice_no = IVC.invoice_no ";

        return selectProductInvoiceInfoStatement;
    }

    public static String selectProductInvoiceInfoByNo(String invNo) {

        String selectProductInvoiceInfoStatement = " SELECT "
                + " IVM.invoice_no, "
                + " IVM.customer_id, "
                + " IVM.customer_name, "
                + " IVM.customer_mobile, "
                + " IVM.customer_address, "
                + " IVM.invoice_status, "
                + " IVM.grand_total, "
                + " IVM.discount, "
                + " IVM.advance, "
                + " IVM.delivery_charge, "
                + " IVM.invoice_by, "
                + " IVM.invoice_date, "
                + " IVC.invoice_child_id, "
                + " IVC.product_name, "
                + " IVC.invoice_size, "
                + " IVC.invoice_size0, "
                + " IVC.invoice_size1, "
                + " IVC.invoice_size2, "
                + " IVC.invoice_size3, "
                + " IVC.invoice_size4, "
                + " IVC.invoice_size5, "
                + " IVC.invoice_size6, "
                + " IVC.invoice_size7, "
                + " IVC.invoice_size8, "
                + " IVC.invoice_size9, "
                + " IVC.item_code, "
                + " IVC.invoice_quantity, "
                + " IVC.invoice_mrp, "
                + " IVC.invoice_sub_total "
                + " FROM "
                + " invoice_master IVM, "
                + " invoice_child IVC "
                + " WHERE "
                + " IVM.invoice_no = IVC.invoice_no "
                + " AND "
                + " IVM.invoice_no = '" + invNo + "' ";

        return selectProductInvoiceInfoStatement;
    }

    public static String selectCustomerInfo() {

        String selectCustomerInfoStatement = " SELECT "
                + " CI.customer_id, "
                + " CI.full_name, "
                + " CI.contact_no, "
                + " CI.dob, "
                + " CI.religion, "
                + " CI.gender, "
                + " CI.hot_line, "
                + " CI.address, "
                + " CI.status "
                + " FROM "
                + " customer_info CI ";

        return selectCustomerInfoStatement;
    }

    public static String selectReturnedProductInfo() {

        String selectReturnedProductInfoStatement = " SELECT "
                + " PRI.item_code, "
                + " PRI.invoice_no, "
                + " PRI.size_unit, "
                + " PRI.return_size0, "
                + " PRI.return_size1, "
                + " PRI.return_size2, "
                + " PRI.return_size3, "
                + " PRI.return_size4, "
                + " PRI.return_size5, "
                + " PRI.return_size6, "
                + " PRI.return_size7, "
                + " PRI.return_size8, "
                + " PRI.return_size9, "
                + " PRI.return_quantity, "
                + " PRI.return_cause, "
                + " PRI.return_status, "
                + " INM.customer_id, "
                + " INM.customer_name, "
                + " INM.customer_mobile, "
                + " INM.customer_address, "
                + " INM.invoice_date "
                + " FROM "
                + " product_return_info PRI, "
                + " invoice_master INM "
                + " WHERE "
                + " PRI.invoice_no = INM.invoice_no ";

        return selectReturnedProductInfoStatement;
    }

    public static String selectExchangedProductInfo() {

        String selectExchangedProductInfoStatement = " SELECT "
                + " EM.invoice_no, "
                + " EM.exchange_cause, "
                + " EM.customer_id, "
                + " EM.customer_name, "
                + " EM.customer_mobile, "
                + " ECH.old_item_code, "
                + " ECH.old_size_unit, "
                + " ECH.old_size0, "
                + " ECH.old_size1, "
                + " ECH.old_size2, "
                + " ECH.old_size3, "
                + " ECH.old_size4, "
                + " ECH.old_size5, "
                + " ECH.old_size6, "
                + " ECH.old_size7, "
                + " ECH.old_size8, "
                + " ECH.old_size9, "
                + " ECH.old_quantity, "
                + " ECH.new_item_code, "
                + " ECH.new_size_unit, "
                + " ECH.new_size0, "
                + " ECH.new_size1, "
                + " ECH.new_size2, "
                + " ECH.new_size3, "
                + " ECH.new_size4, "
                + " ECH.new_size5, "
                + " ECH.new_size6, "
                + " ECH.new_size7, "
                + " ECH.new_size8, "
                + " ECH.new_size9, "
                + " ECH.new_quantity "
                + " FROM "
                + " exchange_master EM, "
                + " exchange_child ECH "
                + " WHERE "
                + " EM.invoice_no = ECH.invoice_no ";

        return selectExchangedProductInfoStatement;
    }

    public static String selectProductDetailsForUpdate(String itemCode) {

        String selectProductDetailsInfoStatement = " SELECT "
                + " PI.pro_item_code, "
                + " PI.pro_name, "
                + " PI.pro_cost_price, "
                + " PI.pro_vat, "
                + " PI.pro_net_price, "
                + " PI.pro_mrp, "
                + " PI.pro_supplier, "
                + " PI.pro_supp_reference, "
                + " PI.pro_state, "
                + " PI.pro_temp, "
                + " PI.pro_life_cycle, "
                + " PI.pro_image_url, "
                + " PI.pro_desc, "
                + " SI.size_code, "
                + " SI.size_unit, "
                + " BI.brand_code, "
                + " BI.brand_name, "
                + " CI.category_code, "
                + " CI.category_name, "
                + " SCI.sub_category_code, "
                + " SCI.sub_category_name, "
                + " COLOR.color_code, "
                + " COLOR.color_name, "
                + " AGE.age_group_code, "
                + " AGE.age_group_name, "
                + " GEND.gender_group_code, "
                + " GEND.gender_group_name "
                + " FROM "
                + " product_info PI, "
                + " size_info SI, "
                + " brand_info BI, "
                + " category_info CI, "
                + " sub_category_info SCI, "
                + " color_info COLOR, "
                + " age_group_info AGE, "
                + " gender_group_info GEND "
                + " WHERE "
                + " PI.size_code = SI.size_code "
                + " AND "
                + " PI.brand_code = BI.brand_code "
                + " AND "
                + " PI.category_code = CI.category_code "
                + " AND "
                + " PI.sub_category_code = SCI.sub_category_code "
                + " AND "
                + " PI.color_code = COLOR.color_code "
                + " AND "
                + " PI.age_group_code = AGE.age_group_code "
                + " AND "
                + " PI.gender_group_code = GEND.gender_group_code "
                + " AND "
                + " PI.pro_item_code = '" + itemCode + "' ";

        return selectProductDetailsInfoStatement;
    }
}
